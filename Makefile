CASE ?= tests/test_init.vader

.PHONY: tests tests_isolated lint test
tests:
	vim -u tests/vimrc '+Vader! tests/*.vader' && echo Success || echo Failure

tests_isolated: $(wildcard tests/test*.vader)

# Requires pipx install vim-vint
lint:
	vint --color --style-problem --verbose --stat $(shell find -type f -iname "*.vim")

test:
	vim -u tests/vimrc '+Vader $(CASE)' && echo Success || echo Failure

tests/%:
	vim -u tests/vimrc '+Vader! $@' && echo Success || echo Failure
