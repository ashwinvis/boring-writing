```txt
 , __                                                                  
/|/  \           o                               o     o               
 | __/ __   ,_       _  _    __,            ,_     _|_     _  _    __, 
 |   \/  \_/  |  |  / |/ |  /  |   |  |  |_/  |  |  |  |  / |/ |  /  | 
 |(__/\__/    |_/|_/  |  |_/\_/|/   \/ \/     |_/|_/|_/|_/  |  |_/\_/|/
                              /|                                    /| 
                              \|                                    \| 
```

Automate boring writing with Vim

## Usage

See the help file in [`doc/boring-writing.txt`](doc/boring-writing.txt) or `:help boring-writing.txt`

## Installation

| Plugin manager | How to install                                                                                                     |
| :------------- | :-------------                                                                                                     |
| [Dein][1]      | `call dein#add('codeberg.org/ashwinvis/boring-writing')`                                                           |
| [minpac][2]    | `call minpac#add('codeberg.org/ashwinvis/boring-writing')`                                                         |
| [Pathogen][3]  | `git clone htps:///codeberg.org/ashwinvis/boring-writing.git ~/.vim/bundle/boring-writing`                         |
| [Plug][4]      | `Plug 'https://codeberg.org/ashwinvis/boring-writing'`                                                             |
| [Vundle][5]    | `Plugin 'https://codeberg.org/ashwinvis/boring-writing'`                                                           |
| Manual         | Copy all of the files into the corresponding subdirectories of `.vim` (or `~/.config/nvim` if you're using Neovim) |

[1]: https://github.com/Shougo/dein.vim
[2]: https://github.com/k-takata/minpac
[3]: https://github.com/tpope/vim-pathogen
[4]: https://github.com/junegunn/vim-plug
[5]: https://github.com/VundleVim/Vundle.vim

## Demo

See the [blogpost][blogpost] or the [test suite](tests) for a demo of the features.

[blogpost]: https://ashwinvis.github.io/automate-boring-writing-with-vim.html 
