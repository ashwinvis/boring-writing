" Description: Vim markdown plugin to automate boring writing
" Repo: https://codeberg.org/ashwinvis/boring-writing

if exists('b:did_boringwriting_md_load')
  finish
endif
let b:did_boringwriting_md_load = 1

"Markdown: inline-link maker
" surround with [], find ], append (
nmap <buffer> <leader>a ysiW]f]a()<ESC>i

"Markdown: link maker
" yank inner word, surround with [], find ], append [], paste word, move right
" mark l, go to end of file, add a line, append [], paste, move right, append:
nmap <buffer> <CR> yiWysiW]f]a[]<ESC>PlmlGo<ESC>a[]<ESC>Pla:<SPACE>

"Markdown: heading maker
" mark h, go to beginning of the line, add #/##/###/### , go back to mark h
nnoremap <buffer> <leader>1 mh^i#<SPACE><ESC>`h
nnoremap <buffer> <leader>2 mh^i##<SPACE><ESC>`h
nnoremap <buffer> <leader>3 mh^i###<SPACE><ESC>`h
nnoremap <buffer> <leader>4 mh^i####<SPACE><ESC>`h

