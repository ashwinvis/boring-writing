" Description: Vim rST plugin to automate boring writing
" Repo: https://codeberg.org/ashwinvis/boring-writing

if exists('b:did_boringwriting_rst_load')
  finish
endif
let b:did_boringwriting_rst_load = 1

" RST: inline-link maker
" surround with ``, find `, append __, move left by 2 characters, insert <>
nmap <buffer> <leader>a ysiW`f`a__<ESC>2hi<SPACE><lt>><ESC>i

" RST: link maker
" yank inner word, go to end of word, append _, mark l, end of document, add a
" new line, insert .. _, paste, append:
nmap <buffer> <CR> yiWEa_<ESC>mlGo<ESC>i..<SPACE>_<ESC>pa:<SPACE>
" RST: heading maker
nnoremap <buffer> <leader>1 yypVr#
nnoremap <buffer> <leader>2 yypVr=
nnoremap <buffer> <leader>3 yypVr-
nnoremap <buffer> <leader>4 yypVr~
