"{{{ Select (visual mode) and search / replace

if has('python3')
python3 << endpython3
import re
import vim

def py_regex_escape(string=None):
    h = string if string else vim.eval('@h')
    h = re.escape(h).replace("'", "''").replace("/", "\/")
    if string:
      print(h)
    else:
      vim.command("let @h='{}'".format(h))

endpython3

  command! -nargs=? RegexEscape :py3 py_regex_escape(<f-args>)
else
  command! RegexEscape :echo "ERROR: This feature of boring-writing plugin requires vim / neovim with Python support"
endif

" http://stackoverflow.com/questions/676600/#676619
" Below h is used as a register to yank into
" Also search with \v prefix for searching with very magic and
" if needed \V prefix with very nomagic. See `:help magic`
vnoremap <C-f> "hy:RegexEscape<CR>/\v<C-r>h
vnoremap <C-r> "hy:RegexEscape<CR>:%s/\v<C-r>h//gc<left><left><left>
"}}}

"{{{ Copy-paste like word processors

"copy in visual mode
vnoremap <C-c> "+y
"paste in insert mode
inoremap <C-v> <C-o>"+gP
"}}}

"{{{ Misc: change directory, file opening
"Change dir to current file
command! CDC lcd %:p:h
nnoremap <leader>cd :CDC<CR>:pwd<CR>

"Vertical split find
command! -nargs=1 -complete=file_in_path V vert sfind <args>
command! -nargs=1 -complete=file_in_path F find <args>
"}}}
